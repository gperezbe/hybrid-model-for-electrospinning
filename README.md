# Hybrid Model for Electrospinning

This repository contains the code implemented for a hybrid model for Electrospinning. It was all done with FeniCs (Python package for solving partial differential equations via the Finite element Method), considering the following version for dolfin: 

dolfin version: 2019.2.0.dev0

All of these codes were implemented on FEM on Colab. FEM on Colab is a collection of packages that allows to easily install several finite element libraries on Google Colab. Currently supported libraries are FEniCS, FEniCSx, firedrake and ngsolve. Mesh generation via gmsh is also supported. 



